# Resuelve FC

Este proyecto es una REST API para el cálculo de sueldos de los jugadores del Resuelve FC, recibe y envía como respuesta un payload en formato JSON como el siguiente:

```
[  
   {  
      "nombre":"Juan Perez",
      "nivel":"C",
      "goles":10,
      "sueldo":50000,
      "bono":25000,
      "sueldo_completo":null,
      "equipo":"rojo"
   },
   {  
      "nombre":"EL Cuauh",
      "nivel":"Cuauh",
      "goles":30,
      "sueldo":100000,
      "bono":30000,
      "sueldo_completo":null,
      "equipo":"azul"
   },
   {  
      "nombre":"Cosme Fulanito",
      "nivel":"A",
      "goles":7,
      "sueldo":20000,
      "bono":10000,
      "sueldo_completo":null,
      "equipo":"azul"

   },
   {  
      "nombre":"El Rulo",
      "nivel":"B",
      "goles":9,
      "sueldo":30000,
      "bono":15000,
      "sueldo_completo":null,
      "equipo":"rojo"

   }
]
``` 

El sueldo completo de cada jugador esta determinado por un sueldo fijo y un bono variable, el bono se calcula con base a los goles mínimos por equipo y los goles individuales anotados, dependiendo del nivel asignado en el equipo. Cada cifra de goles tiene un peso de 50% respecto al bono y se suma al sueldo fijo. 

## Getting Started

Para comenzar, asegurate de tener instalado java 8 en tu equipo, puedes instalarlo de manera fácil con el gestor:

* [SDKMAN](https://sdkman.io/)

Posteriormente, debes clonar el proyecto a tu ambiente de trabajo

```
git clone git@gitlab.com:bluesrapper/spring-resuelve.git
``` 

Puedes empezar a desarrollar con el editor de código de tu preferencia o con cualquier IDE que pueda importar proyectos gradle.

Para probar el servicio, ejecuta el siguiente comando sobre la raíz del proyecto:

```
./gradlew bootRun
```

Por defecto, el servidor se levanta sobre el puerto 8080, por lo que para realizar un request de prueba puedes ejecutar el siguiento comando:

```
curl --location --request GET 'http://localhost:8080/sueldo/resuelve-fc' --header 'Content-Type: application/json' --data-raw '*aquí va tu payload*'
```

ó

```
curl --location --request GET 'http://localhost:8080/sueldo/bonus' --header 'Content-Type: application/json' --data-raw '*aquí va tu payload*'
```

## Ejecución de Pruebas Unitarias

Para ejecutar las pruebas unitarias, ejecuta el siguiente comando sobre la raíz del proyecto:

```
./gradlew test
```

## Construcción y Ejecución del Servicio

Para construir el JAR del proyecto, ejecuta el siguiente comando sobre la raíz del proyecto:

```
./gradlew build
```

Una vez que termine el proceso, pueden accesar a la carpeta build/libs donde encontrarás el artifact generado.

Para ejecutar el servicio, ejecuta el siguiente comando:

```
java -jar <<nombre-del-artifact.jar>>
```

## Producción y pruebas

El proyecto cuenta con integración continua, al desplegar el proyecto la url de producción es la siguiente:

* [Resuelve FC Productivo](https://resuelve-fc-prod.herokuapp.com)

Para probar cambios, realiza un push a la rama *pruebas* y apunta tu request a la siguiente url:

* [Resuelve FC Pruebas](https://resuelve-fc-dev.herokuapp.com)

## Tecnologías de Construcción

* [Spring Boot](https://spring.io/projects/spring-boot) - Framework
* [Gradle](https://gradle.org/) - Gestor de dependencias

## Recomendación de conocimientos

* [Git](https://git-scm.com/doc) - Documentación para manejo de git
* [Groovy](http://groovy-lang.org/documentation.html) - Lenguaje de Programación