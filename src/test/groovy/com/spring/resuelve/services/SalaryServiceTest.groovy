package com.spring.resuelve.services

import org.junit.jupiter.api.Test
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest

@SpringBootTest
class SalaryServiceTest {

    @Autowired
    SalaryService salaryService

    @Test
    void "Porcentaje de equipo"() {
        BigDecimal percentage

        when: 'El número de goles es mayor al total de goles de los alcances'
            percentage = salaryService.getTeamBonus(50, 60)
        then:
            assert percentage == 1

        when: 'El número de goles es negativo'
            percentage = salaryService.getTeamBonus(50, -10)
        then:
            assert percentage == 0

        when: 'El número de goles esta dentro de rango'
            percentage = salaryService.getTeamBonus(50, 25)
        then:
            assert percentage == 0.5
    }

    @Test
    void "Porcentaje de bono por jugador, cuando el nivel existe"() {
        BigDecimal percentage

        when: 'El número de goles es mayor al requerido para su nivel'
            percentage = salaryService.getPlayerBonus(10, "A")
        then:
            assert percentage == 1

        when: 'El número de goles es negativo'
            percentage = salaryService.getPlayerBonus(-2, "C")
        then:
            assert percentage == 0

        when: 'El porcentaje esta dentro de rango'
            percentage = salaryService.getPlayerBonus(4, "B")
        then:
            assert percentage == 0.4
    }

    @Test
    void "Porcentaje de bono de jugador cuando el nivel no existe"() {
        BigDecimal percentage

        when:
            percentage = salaryService.getPlayerBonus(13, "Cuac")
        then:
            assert percentage == 0
    }

    @Test
    void "Cálculo de sueldo"() {
        BigDecimal salary

        when: 'Se cumple la meta de goles por equipo y la personal'
            salary = salaryService.getFullSalary(30, 'Cuauh', 45000 as BigDecimal, 22500 as BigDecimal, 1 as BigDecimal)
        then:
            assert salary == 67500

        when: 'No se cumplen las metas de goles por equipo ni la personal'
            salary = salaryService.getFullSalary(10, 'C', 10000 as BigDecimal, 7000 as BigDecimal, 0.8 as BigDecimal)
        then:
            assert salary == 15145

        when: 'Cumple la meta de goles por equipo, pero no la personal'
            salary = salaryService.getFullSalary(3, 'A', 7000 as BigDecimal, 3500 as BigDecimal, 1 as BigDecimal)
        then:
            assert salary == 9800

        when: 'No se cumple la meta de goles por equipo, pero la personal si'
            salary = salaryService.getFullSalary(7, 'B', 16500 as BigDecimal, 8250 as BigDecimal, 0.75 as BigDecimal)
        then:
            assert salary == 22481.25
    }

    @Test
    void "Cálculo de sueldo de jugadores"() {
        def salaries

        when: 'El payload tiene una estructura correcta'
            def players = [
                [ nombre: "Juan Perez",     nivel: "C",     goles: 10, sueldo: 50000,  bono: 25000, sueldo_completo: null, equipo: "rojo" ],
                [ nombre: "EL Cuauh",       nivel: "Cuauh", goles: 30, sueldo: 100000, bono: 30000, sueldo_completo: null, equipo: "azul" ],
                [ nombre: "Cosme Fulanito", nivel: "A",     goles: 7,  sueldo: 20000,  bono: 10000, sueldo_completo: null, equipo: "azul" ],
                [ nombre: "El Rulo",        nivel: "B",     goles: 9,  sueldo: 30000,  bono: 15000, sueldo_completo: null, equipo: "rojo" ]
            ]

            salaries = salaryService.getSalaries(players)
        then:
            assert salaries == [
                [ nombre: "Juan Perez",     goles_minimos: 15, goles: 10, sueldo: 50000,  bono: 25000, sueldo_completo: 67875,  equipo: "rojo" ],
                [ nombre: "El Rulo",        goles_minimos: 10, goles: 9,  sueldo: 30000,  bono: 15000, sueldo_completo: 42450,  equipo: "rojo" ],
                [ nombre: "EL Cuauh",       goles_minimos: 20, goles: 30, sueldo: 100000, bono: 30000, sueldo_completo: 130000, equipo: "azul" ],
                [ nombre: "Cosme Fulanito", goles_minimos: 5,  goles: 7,  sueldo: 20000,  bono: 10000, sueldo_completo: 30000,  equipo: "azul" ]
            ]
    }
}
