package com.spring.resuelve.services

import org.junit.jupiter.api.Test
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest

@SpringBootTest
class UtilServiceTest {

    @Autowired
    private UtilService utilService

    @Test
    void "Un número tiene como máximo 2 dígitos decimales"() {
        BigDecimal numero

        when: 'El número contiene mas de 2 dígitos decimales, con redondeo hacia arriba'
            numero = utilService.formatNumber(18.666)
        then:
            assert numero == 18.67

        when: 'El número contiene mas de 2 dígitos decimales, con redondeo hacia abajo'
            numero = utilService.formatNumber(15.311111)
        then:
            assert numero == 15.31
    }

    @Test
    void "Los números con 2 o menos dígitos decimales devuelven la misma cantidad"() {
        BigDecimal numero

        when: 'El número contiene 2 digitos decimales'
            numero = utilService.formatNumber(45.34)
        then:
            assert numero == 45.34

        when: 'El número contiene menos de 2 digitos decimales'
            numero = utilService.formatNumber(78.1)
        then:
            assert numero == 78.10
    }
}
