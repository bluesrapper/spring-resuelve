package com.spring.resuelve.controllers

import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController

/**
 * Clase controladora para mostrar mensaje de bienvenida a la API
 *
 * @author Octavio Juárez Molina
 * @version 1
 */
@RestController
class IndexController {

    /**
     * Mensaje de bienvenida a la API sobre el contexto raiz
     *
     * @method GET
     * @return
     */
    @RequestMapping("/")
    def index() {
        [ msg: "Bienvenido a la API para cálculo de sueldo del Resuelve FC" ]
    }
}
