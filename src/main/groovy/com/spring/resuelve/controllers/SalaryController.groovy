package com.spring.resuelve.controllers

import groovy.util.logging.Slf4j
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController

import com.spring.resuelve.services.SalaryService

/**
 * Clase controladora que recibe la información necesaria de los jugadores para calcular los sueldos completos dado un sueldo base y un tope de bono
 * @author Octavio Juárez Molina
 * @version 1
 */
@Slf4j
@RestController
@RequestMapping("/sueldo")
class SalaryController {

    @Autowired
    SalaryService salaryService

    /**
     * Recibe el payload con la información de los jugadores que se desea calcular su sueldo completo
     * @param players Payload en formato JSONArray
     * @return
     */
    @RequestMapping("/resuelve-fc")
    def index(@RequestBody Map<String, Object>[] players) {
        def response = []

        if(!players) {
            return [ error: "No se recibió el payload esperado" ]
        }

        if(!validatePayload(players)) {
            return [ error: "La información recibida no cumple con el tipo de datos necesario" ]
        }

        salaryService.getSalaries(players)
    }

    /**
     * Recibe el payload con la información de los jugadores que se desea calcular su sueldo completo, además de
     * recibir la tabla de metas por gol según su nivel
     * @param payload Información de jugadores y metas en el mismo objeto
     * @return
     */
    @RequestMapping("/bonus")
    def bonus(@RequestBody Object payload) {
        def response = []

        if(!payload) {
            return [ error: "No se recibió el payload esperado" ]
        }

        if(!validatePayload(payload.jugadores) || !validatePayloadTargets(payload.metas)) {
            return [ error: "La información recibida no cumple con el tipo de datos necesario" ]
        }

        salaryService.targets = payload.metas
        salaryService.getSalaries(payload.jugadores as Map<String, Object>)
    }

    /**
     * Recibe un arreglo de jugadores para validar la información necesaria y determinar si es posible procesar el cálculo de sueldo completo
     * @param players Arreglo de jugadores para calcular sus sueldos
     * @return
     */
    protected boolean validatePayload(def players) {
        boolean isValid = false

        try {
            players.each { player ->
                (player.goles ?: '') as int
                (player.sueldo ?: '') as BigDecimal
                (player.bono ?: '') as BigDecimal
            }

            isValid = true
        } catch(Exception e) {
            log.error e.message
        }

        isValid
    }

    /**
     * Recibe un arreglo de metas para validar la información necesaria para procesar el cálculo de sueldo completo
     * @param targets Arreglo de metas para calcular bonos
     * @return
     */
    protected boolean validatePayloadTargets(def targets) {
        boolean isValid = false

        try {
            targets.each { target ->
                (target.goles ?: '') as int
            }

            isValid = true
        } catch(Exception e) {
            log.error e.message
        }

        isValid
    }
}
