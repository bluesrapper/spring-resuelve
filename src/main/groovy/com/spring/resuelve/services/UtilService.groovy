package com.spring.resuelve.services

import groovy.util.logging.Slf4j
import org.springframework.stereotype.Service

import java.text.NumberFormat

/**
 * Clase destinada para el almacenamiento de métodos de ayuda general
 * @author Octavio Juárez Molina
 * @version 1
 */
@Slf4j
@Service
class UtilService {

    /**
     * Método encargado de devolver un número en formato a 2 digitos decimales
     *
     * @param number Número a dar formato
     * @return {BigDecimal} Número con formato a 2 dígitos decimales
     */
    BigDecimal formatNumber(def number) {
        BigDecimal formatNumber = 0
        NumberFormat numberFormat = NumberFormat.numberInstance

        numberFormat.maximumFractionDigits = 2
        numberFormat.minimumFractionDigits = 2
        numberFormat.applyPattern "###.##"

        try {
            formatNumber = numberFormat.format(number) as BigDecimal
        } catch(NumberFormatException nfe) {
            log.error nfe.message
        }

        formatNumber
    }
}