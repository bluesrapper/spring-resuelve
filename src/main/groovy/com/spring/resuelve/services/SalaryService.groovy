package com.spring.resuelve.services

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service

/**
 * Servicio para atender la lógica de negocio para cálculo de porcentajes de bono y sueldos de jugadores
 * @author Octavio Juárez Molina
 * @version 1
 */
@Service
class SalaryService {

    @Autowired
    UtilService utilService

    Set targets = [
        [ nivel: "A", goles: 5 ], [ nivel: "B", goles: 10 ], [ nivel: "C", goles: 15 ], [ nivel: "Cuauh", goles: 20 ]
    ]

    /**
     * Calcula el sueldo de los jugadores, considerando sueldo base, bono y nivel de cada jugador.
     * @param players Mapa de jugadores
     * @return
     */
    def getSalaries(def players) {
        def response = []
        List teams = players*.equipo.unique() // Se obtienen los equipos de forma individual con base a la lista de jugadores

        teams.each { team ->

            int scored = players.findAll{ it.equipo == team }.goles.sum() // Se obtiene el número de goles anotados por equipo
            Set levels = players.findAll{ it.equipo == team }*.nivel
            int expected = (targets.findAll{ it.nivel in levels }?.goles?.sum() ?: 0) as int
            BigDecimal bonus = this.getTeamBonus(expected, scored)

            players.findAll { it.equipo == team }.each { player ->

                BigDecimal fullSalary = this.getFullSalary(player.goles as int, player.nivel as String, player.sueldo as BigDecimal, player.bono as BigDecimal, bonus)
                int minimum = (targets.find { it.nivel == player.nivel }?.goles ?: 0) as int

                response << [
                    nombre         : player.nombre,
                    goles_minimos  : minimum,
                    goles          : player.goles,
                    sueldo         : player.sueldo,
                    bono           : player.bono,
                    sueldo_completo: fullSalary,
                    equipo         : player.equipo
                ]
            }
        }

        response
    }

    /**
     * Calcula el sueldo de un jugador, tomando en cuenta los goles anotados y el bono por equipo
     * @param goals Goles anotados por el jugador
     * @param level Nivel asociado al jugador
     * @param salary Salario base
     * @param bonus Bono asociado
     * @param teamBonus Bono por equipo
     * @return
     */
    BigDecimal getFullSalary(int goals, String level, BigDecimal salary, BigDecimal bonus, BigDecimal teamBonus) {
        BigDecimal playerBonus = this.getPlayerBonus(goals, level)
        BigDecimal fullSalary = salary + (bonus * ((playerBonus + teamBonus) / 2))

        utilService.formatNumber(fullSalary)
    }

    /**
     * Calcula el bono por equipo expresado en decimal, recibe el total de goles esperados y el total de goles anotados.
     * @param expected Total de goles esperados por el equipo
     * @param scored Total de goles anotados por el equipo
     * @return
     */
    BigDecimal getTeamBonus(int expected, int scored) {
        BigDecimal percentage = (scored / expected) as BigDecimal
        (percentage < 0 ? 0 : percentage > 1 ? 1 : utilService.formatNumber(percentage)) as BigDecimal
    }

    /**
     * Calcula el bono individual dependiendo del nivel del jugador.
     * @param scored Total de goles anotados por el jugador
     * @param level Nivel del jugador
     * @return
     */
    BigDecimal getPlayerBonus(int scored, String level) {
        int expected = (targets.find{ it.nivel == level }?.goles ?: 0) as int
        BigDecimal percentage = (expected ? scored / expected : 0) as BigDecimal

        (percentage < 0 ? 0 : percentage > 1 ? 1 : utilService.formatNumber(percentage)) as BigDecimal
    }
}
