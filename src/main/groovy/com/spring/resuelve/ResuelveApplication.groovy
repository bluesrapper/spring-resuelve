package com.spring.resuelve

import org.springframework.boot.SpringApplication
import org.springframework.boot.autoconfigure.SpringBootApplication

@SpringBootApplication
class ResuelveApplication {

	static void main(String[] args) {
		SpringApplication.run(ResuelveApplication, args)
	}

}
